<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use KDuma\Permissions\Permissions;
use Sofa\Revisionable\Laravel\RevisionableTrait;
use Sofa\Revisionable\Revisionable;


/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\config('permissions.models.Role[] $roles 
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Revisionable\Laravel\Revision[] $revisions 
 * @property-read \Sofa\Revisionable\Laravel\Revision $revisionsCount 
 * @property-read mixed $revisions_count 
 * @property-read \Sofa\Revisionable\Laravel\Revision $oldestRevision 
 * @property-read mixed $oldest_revision 
 * @property-read \Sofa\Revisionable\Laravel\Revision $latestRevision 
 * @property-read mixed $latest_revision 
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, Revisionable
{
    use Authenticatable, CanResetPassword, Permissions;

    use RevisionableTrait;

    /*
     * Set revisionable whitelist - only changes to any
     * of these fields will be tracked during updates.
     */
    protected $revisionable = [
        'email',
        'name',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}
